﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client
{
    class Snake
    {
        public int Length
        {
            get
            {
                return location.Count;
            }
        }

        private List<Point> location;

        public Point[] Location
        {
            get { return location.ToArray(); }
        }

        public Snake()
        {
            location = new List<Point>();
            Reset();
        }

        public void Reset()
        {
            for (int i = 0; i < 2; i++)
            {
                location.Add(new Point() { X = 12, Y = 12 });
            }
        }

        public void Relocate()
        {
            for (int i = location.Count-1; i > 0; i--)
            {
                location[i] = location[i - 1];
            }
        }

        public void Up()
        {
            Relocate();
            Point temp = location[0];
            int x = temp.X;
            int y = temp.Y - 1;
            if (location[0].Y < 0)
            {
                y = 50;
            }

            location[0] = new Point() { X = x, Y = y };
        }

        public void Down()
        {
            Relocate();
            Point temp = location[0];
            int x = temp.X;
            int y = temp.Y + 1;
            if (y > 49)
            {
                y -= 50;
            }

            location[0] = new Point() { X = x, Y = y };
        }

        public void Left()
        {
            Relocate();
            Point temp = location[0];
            int x = temp.X - 1;
            int y = temp.Y;
            if (x < 0)
            {
                x += 50;
            }

            location[0] = new Point() { X = x, Y = y };
        }
        public void Right()
        {
            Relocate();
            Point temp = location[0];
            int x = temp.X + 1;
            int y = temp.Y;
            if (x > 49)
            {
                x -= 50;
            }

            location[0] = new Point() { X = x, Y = y };
        }

        public void incLength()
        {
            location.Add(new Point());
            Relocate();
        }
    }
}
