﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using SnakeContract;
using System.Drawing;

namespace Client
{
    public static class Singleton
    {
        public static DuplexChannelFactory<ISnakeContract> Comm { get; set; }
        public static ISnakeContract CommObj { get; set; }
        public static int IndexOfThePlayer { get; set; }
        public static Point[] Obstacles { get; set; }
    }
}
