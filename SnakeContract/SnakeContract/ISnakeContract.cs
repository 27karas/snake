﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ServiceModel;

namespace SnakeContract
{
    //returns messages to clients
    [ServiceContract (CallbackContract = typeof(ICallback))]
    public interface ISnakeContract
    {
        [OperationContract(IsOneWay = true)]
        void Connect(string name);

        [OperationContract(IsOneWay = true)]
        void Deconnect();

        [OperationContract(IsOneWay = true)]
        void SendLocation(Point[] location, Point meal);

        [OperationContract(IsOneWay  = true)]
        void SendMessage(string text) ;

        
    }
}
